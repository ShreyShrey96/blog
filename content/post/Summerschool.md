---

title: Summerschool begint!
date:  2018-06-18

---

Vandaag was het een drukke dag. Ik had in de ochtend eerst een teken tentamen en had hierna gelijk de kickoff van het summerschool project. Ik heb in het afgelopen assessment helaas moeten accepteren dat ik niet al mijn competenties heb behaald. Dit kan ik dus nu allemaal herkansen in de summerschool. Bij deze kickoff werden we echt overladen aan informatie. Er staat mij veel te doen voor deze komende twee weken en de energie hiervoor begint te groeien. Ik zie dit als een tweede kan en een fresh-start. Met de kickoff werd er gelijk bekend gemaakt hoe we gaan werken en wat het project precies inhoud. Er is hierbij ook nog vertelt dat er een gezamenlijk onderzoek zal zijn en dat het gehele project is ontworpen om zoveel mogelijk competenties te kunnen behalen. Na de kickoff moesten wij al gelijk bedenken wat de locatie/doelgroep/bedrijf is waar ik onderzoek naar moest doen. Na lang nadenken en beetje rond te hebben gekeken op internet ben ik uitgekomen op Den Haag en als bedrijf/instelling de gemeente van Den Haag. Mijn doelgroep heb ik nog mijn twijfels over maar ik denk nu aan toeristen en de dagelijkse reiziger die Den Haag bezoekt.