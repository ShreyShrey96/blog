---
title: terugblik Expo
date: 2018-06-03
---

De expo is achter de rug en we zijn allemaal erg tevreden zelf ons teamlid die met de expo niet aanwezig kon zijn heeft met ons meegejuicht en ons succes gewenst. Alles tijdens de expo is goed gevalideerd en mensen die langs kwamen vonden ons concept erg leuk en interessant.  
Alleen toen EMI langs kwam waren ze ondanks hun enthousiasme over het concept wel een beetje verbaasd over het kostenplaatje. 
De periode is nu voorbij en we zijn ons nu aan het focussen op het leerdossier, want dit moet morgenochtend ingeleverd worden! Ik heb erg mijn best gedaan deze twee periodes en kijk hier graag weer op terug en ik hoop dat al het werk ook voldoende zal zijn en ik dit jaar met een gerust hart kan afsluiten.
