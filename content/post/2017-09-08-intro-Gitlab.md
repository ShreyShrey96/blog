---
title: introductie Gitlab
date: 2017-09-08
---
Vandaag hebben wij in de tweede studiodag in deze week een introductie gehad over het maken van een blog met Gitlab. Ik had hierbij grote verwachtingen en dacht dat het nog best uitdagend en tijd vretend zou zijn, maar dit was echter niet zo. We gaan werken met markdown bestanden. Hierin is er zo min mogelijk code nodig en kan je eigenlijk zeer eenvoudig zonder enige moeite je blog posten. Ik was hier enigszins toch wel blij mee omdat je nu zo even snel tussen een les door of in een pauze een blog kan posten. We hebben na deze cursus over het blog ook een SLC-les gehad waarin de SWOT-analyse werd besproken. Dit vond ik wel leuk omdat ik dit afgelopen jaar op mijn vorige studie ook had en het gaf mij dus een gevoel van herkenning. 
Verder vandaag heb ik met het team onderzoek gedaan over bestaande bordspellen en hebben we gekeken hoe Rotterdam gecombineerd kan worden met de aspecten die de bestaande spellen bevatten. Hieruit hebben wij toch wat antwoorden gekregen en is er wat helder beeld ontstaan over ons spel.
