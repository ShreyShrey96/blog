---

title: Day 3 - research day!
date: 2018-06-20

---

Vandaag ben in naar Den Haag gegaan voor onderzoek naar fietsen en hoe de stad hier gebruik van maakt. Dit was een observatie voor de touchpoints binnen het systeem van de ov-fiets. Hierna heb ik ook nog een aantal mensen gesproken en hun vragen gesteld over de systemen die onder andere fietsen verhuren. Ik was in dit onderzoek niet alleen naar Den Haag maar Tyoni en Prewish waren ook mee en we hebben samen info verzameld wat ieder zou helpen voor zijn onderzoek. 
Voordat we naar Den Haag gingen waren we eerst nog op school bezig meet het onderzoeksplan en heb ik heb hier feedback bij gevraagd aan Fedde. Na dit gedaan te hebben heb ik ook nog de samenwerking presentatie van Josephin en Lisanne. Hier hebben we gezamenlijk met Isaiah, Prewish, Tyoni, Josephin en Lisanne een creatieve sessie gehouden. Na de sessie zijn Prewish, Tyoni en ik naar Den Haag gegaan en hebben we onze fieldresearch gehouden. Na de fieldresearch was de dag al weer om en zijn we naar huis gegaan en heb ik thuis nog alle info uitgeschreven in een onderzoeksrapport.
