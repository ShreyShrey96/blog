---
title: De opdracht
date: 2017-11-20
---

De opdracht is dat wij een digitaal product (het liefst een clickable prototype) moeten maken voor een van de vier doelgroepen die we hebben gekregen: de Oldskool, de Selecteur, de Nieuweling en de Ontdekker. Het product moet meermaals grondig worden getest en heeft onderbouwend visueel zijn met interactieontwerp. Het product moet de 5 core principles aantonen: user-centered aanpak, merkgedreven design, onverwachte oplossingen, why, how, what en een gevalideerd onderzoek. De opdrachtgever wil ook dat wij het proces aantonen met kleinere opdrachten zoals een user journey, een aantal low fid prototypes en andere dingen. De 3 beste concepten mogen presenteren in Paard en krijgen een grotere kans op een stageplek mede mogelijk gemaakt door Fabrique.