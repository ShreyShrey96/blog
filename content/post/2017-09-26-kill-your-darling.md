---
title: kill your darling
subtitle: noooooooooo
date: 2017-09-26
---

Afgelopen week ging iteratie 2 van start. De iteratie heet kill your darling. Dit houdt in dat we het gehele concept van iteratie 1 moeten laten vallen en opnieuw een concept moeten bedenken. Dit was toen een redelijke schok, maar we vonden dit niet zo erg aangezien we vast zaten om het offline aspect ook een online aspect te bepalen. Vandaag gingen wij dus kijken naar wat wij zouden kunnen bedenken voor ons nieuwe concept. We hadden wel vastgesteld om een app te maken met een augmented reality. Maar hoe en wat is nog niet duidelijk. We hebben ook de feedback meegenomen over iteratie 1. 