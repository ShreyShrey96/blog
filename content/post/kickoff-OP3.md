---
title: Kickoff OP3 
date: 2018-02-06
---

Vandaag was de kickoff van het derde kwartaal hier kregen we op de vijfde verdieping bij wijnhaven een kleine intro over de planning en het project voor de komende periode. Dit project vind ik echt spannend worden want het is nu niet een project voor alleen periode 3 maar ook voor periode 4. Ik kreeg echt een motivatie boost hierdoor en heb echt zin gekregen in het project. Naast de kickoff kregen de mensen die mee ging naar Maastricht ook nog wat meer info over de dag van vertrek en daarna was deze hoorcollege weer voorbij en konden we door naar ons keuzevak. Het keuzevak dat ik deze periode heb gekozen is “navigeren met sterren” en ik ben echt blij met dit keuzevak. De docent vertelt echt met volle passie over de sterren en zijn verhalen erachter. Ik zelf had ook altijd wel wat interesse in de sterren en hou wel van de mythologie dus vond ik dit keuzevak een leuke ervaring om mee te maken.