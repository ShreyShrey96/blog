---
title: eerste studiodag!
date: 2017-09-05

---

Vandaag heeft onze klas uitleg gehad over het paper prototype. Het was onze eerste studiodag! Op deze dagen mogen wij werken aan ons project. Wij hadden inspiratie nodig en hebben deze dag dus door Rotterdam gelopen en inspiratie opgezocht. Hier heb ik toch geleerd dat je veel tegenkomt in een stad waar ik mijn hele leven al kom.
Na de wandeling door Rotterdam hebben wij in de studio fotocollages in elkaar gezet. Deze hebben we naast elkaar gezet en begonnen te vergelijken om zo een thema eruit te halen met een goeie doorloop. Wij kwamen uiteindelijk op het idee om een spel te maken met iconische aspecten van de stad erin. We begonnen te kijken naar het offline aspect en dachten aan een bordspel.
