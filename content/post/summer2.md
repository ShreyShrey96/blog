---

title: Day 2
date: 2018-06-19

---

Vandaag is de officiele eerste dag van summerschool. We begonnen de dag met de dagelijkse intro presentatie. Hier kregen we te horen om in een groep een verbredend onderzoek uit te voeren naar het verhuursysteem. Hoe werkt het en wat zijn de voordelen en nadelen van deze systemen. Dit was het hoofddoel om uit te zoeken voor vandaag. Hiervoor mochten wij kiezen uit een klein aantal bedrijven: de ov-fiets, de swapfiets en Greenwheels. Ik werkte samen met Isaiah, Prewish en Lindsay. Wij hadden gekozen voor de ov en hebben hierbij een snel onderzoek uitgevoerd in Rotterdam Centraal. Na wat informatie te hebben verzameld en interviews hebben uitgevoerd zijn we teruggegaan naar school. Op school hebben wij met een andere groep elkaars informatie uitgewisseld. Hierna is iedereen gestart met het opzetten van hun eigen onderzoeksplan en daarna was de dag min of meer afgelopen op school. 
Na school had ik mijn keuzevak Spaans. Dit was de laatste les en we hebben hier alle stof herhaald ter voorbereiding op het tentamen volgende week. Na dit keuzevak ben ik naar huis gegaan en heb ik nog wat gewerkt aan het onderzoek. Ik vind wel echt fijn dat sinds de start van summerschool alles concreet is doorgegeven en ik dus makkelijk vooruit kan werken in de planning. Morgen ga ik verder met het onderzoek in Den Haag.
