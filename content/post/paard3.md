---
title: de eerste dag
date: 2017-11-24
---

Op vrijdag werden de groepjes gevormd. Dit ging door middel van je rol in de vorige groep. Ik was in periode 1 de monitor van de groep en ging met mensen werken die een andere rol hebben gehad. Ik heb een groepje gevormd met Robin, Isaiah, Shreyas, Steffen en Lisanne en we hebben het de naam ‘Smooth Operators’ gegeven. Na het vormen van de groepjes hebben wij onze doelgroep (Selecteur) gekregen zijn de taken binnen het team verdeeld. Ik ben de notulist van die groep; dat houdt in dat ik aantekeningen maak van alles wat we gaan doen. Nadat de taken zijn verdeeld hebben we een samenwerkingscontract samengesteld, een SWOT-analyse van ons team gemaakt en gebrainstormd. Volgende week moet elk persoon met drie ideeën komen voor een concept.