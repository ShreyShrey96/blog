---
title: Start OP2 Paard
date: 2017-11-16
---

Op dinsdag werd periode 2 geopend in Het Paard van Troje, een concertzaal in het centrum van Den Haag. Hier hebben wij onze opdracht gekregen van Paard zelf en Fabrique. Voor de opdracht hebben ze ons de kernvraag gegeven: “Hoe kan Paard bezoekers stimuleren om vaker terug te komen?”. Hierbij moeten wij letten op hun 4 belangrijkste punten: Geluk (mensen gelukkig maken met muziek), Trots (nog steeds de enige concertzaal in en rondom Den Haag), Intiem (Paard is er voor jou) en Verrassing (Paard verrijkt jou en roept emoties op).