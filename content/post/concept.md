---
title: a concept was born
date: 2018-03-16
---

De presentatie is geweest en het ging best goed. We hebben goeie feedback verkregen en waren best tevreden hiermee waardoor we gelijk na de presentatie zijn gaan chillen bij Doppio en hier zijn wezen brainstormen met z’n alle over een concept. Hierbij kwam er een heel mooi concept uit wat de school zou moeten veranderen in een “green school”. In deze school is er overal groen te zien en wordt de gemiddelde gezondheid gerepresenteerd aan de hand van visuals op schermen door de school heen. De student heeft dan de uiterlijk en vrolijkheid van de school in eigen hand. Wij zijn ervan overtuigd dat dit een goede stimulans is voor mensen om gezonder te gaan leven.   