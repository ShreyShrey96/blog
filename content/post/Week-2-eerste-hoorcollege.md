---
title: Eerste hoorcollege OP3
date: 2018-02-12
---

Vandaag was het nieuwe hoorcollege van de derde periode en het ging over artefacten. Dit was een lang college waarin veel geschiedenis werd besproken over het belang van materiaal en wat voor ontwikkeling het voort bracht bij het ontdekken van een nieuw materiaal destijds. Ik vond dit erg interessant omdat het liet zien wat een artefact voor effect kan krijgen op de wereld. Hierna heb ik met medestudenten een beetje gepraat over het project en daarna was de dag alweer om.