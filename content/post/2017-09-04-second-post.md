---
title: Hoorcollege  
date: 2017-09-04
---

Vandaag hebben we een hoorcollege gehad van onze docent Raúl Martínez-Orozco. Dit college ging over het ontwerpproces. Hier leerde we eerst de betekenis achter design. Wat is design nu precies? Hij liet ons zien dat design toch wel een grotere betekenis had dan wat veel dachten. Een design is een proces. In een proces hebben we een ‘input’ en een ‘output’. Hiertussen bestaat het proces dat in een aantal stappen of bepaalde volgorde wordt uitgevoerd. We gaan van een huidige situatie naar een gewenste situatie. En in dit proces houden wij ons aan het ontwerpproces. In dit proces hebben we een driehoek met daarin het onderzoek(O), het concept(C) en het prototype (P).  Deze drie aspecten zijn in het proces noodzakelijk. Dit heeft Raúl ons allemaal uitgelegd en heb ik allemaal in mijn aantekeningen gezet. Ik vond dit een leuk hoorcollege en niet al te lang, maar heb toch veel kunnen leren. 
Aan het eind van het hoorcollege kwam het project ter sprake en werd de opdracht ons uitgelegd. We hebben hier een briefing ontvangen met daarin kort uitleg over het project en de deliverables voor de eerste iteratie. Dit vond ik zelf echt goed van de studie, want het gaf mij gelijk een goed overzicht over wat komen gaat. 
